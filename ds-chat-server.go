package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type DSChatServer struct {
	HTTPServer *http.Server
	Clients    map[string]bool
}

func NewDSChatServer() *DSChatServer {
	dscs := new(DSChatServer)
	router := mux.NewRouter()
	router.HandleFunc("/signin", dscs.SignInHandler).Methods("POST")
	router.HandleFunc("/nicknames", dscs.ListNicknamesHandler).Methods("GET")
	dscs.HTTPServer = &http.Server{
		Handler: router,
		Addr:    ":8080",
	}
	dscs.Clients = map[string]bool{}
	return dscs
}

func (dscs *DSChatServer) SignInHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Panicln(err.Error())
		return
	}
	nickname := string(body)
	if _, exists := dscs.Clients[nickname]; exists {
		w.WriteHeader(http.StatusForbidden)
	} else {
		dscs.Clients[nickname] = false
		w.WriteHeader(http.StatusCreated)
		log.Printf("Nickname '%s' registered.\n", nickname)
	}
}

func (dscs *DSChatServer) ListNicknamesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	nicknames := []string{}
	for nickname := range dscs.Clients {
		nicknames = append(nicknames, nickname)
	}
	encoder := json.NewEncoder(w)
	err := encoder.Encode(nicknames)
	if err != nil {
		log.Panicln(err)
		return
	}
}

func (dscs *DSChatServer) Run() {
	err := dscs.HTTPServer.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
