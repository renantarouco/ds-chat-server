package main

func main() {
	dscs := NewDSChatServer()
	dscs.Run()
}

/*
type Message struct {
	Nickname string `json:"nickname"`
	Body     string `json:"body"`
}

var (
	upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	clients   = map[*websocket.Conn]bool{}
	broadcast = make(chan Message)
)

func main() {
	http.HandleFunc("/ws", ConnectionHandler)
	go MessagesHandler()
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func ConnectionHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal("Upgrade: ", err)
	}
	defer conn.Close()
	clients[conn] = true
	for {
		var msg Message
		err := conn.ReadJSON(&msg)
		if err != nil {
			log.Print("ReadJSON: ", err)
			delete(clients, conn)
			break
		}
		broadcast <- msg
	}
}

func MessagesHandler() {
	for {
		msg := <-broadcast
		for conn := range clients {
			err := conn.WriteJSON(msg)
			if err != nil {
				log.Print("WriteJSON: ", err)
				conn.Close()
				delete(clients, conn)
			}
		}
	}
}
*/
